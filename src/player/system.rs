use amethyst::{
    core::Transform,
    ecs::{Join, Read, ReadExpect, ReadStorage, System, WriteExpect, WriteStorage},
    input::{InputHandler, StringBindings},
};

use crate::player::{Player, PositionResource as PlayerPosition};

pub struct PlayerSystem;

impl<'a> System<'a> for PlayerSystem {
    type SystemData = (
        WriteStorage<'a, Transform>,
        ReadStorage<'a, Player>,
        Read<'a, InputHandler<StringBindings>>,
        WriteExpect<'a, PlayerPosition>,
    );

    fn run(&mut self, (mut transforms, player, input, mut position_resource): Self::SystemData) {
        for (_player, transform) in (&player, &mut transforms).join() {
            // TODO refine movement
            // * physics based movement
            //
            // move by some scalar
            let scalar = 1.8;
            let movement = (
                input.axis_value("player_x").unwrap_or(0.0) * scalar,
                input.axis_value("player_y").unwrap_or(0.0) * scalar,
            );

            transform.append_translation_xyz(movement.0, movement.1, 0.0);
            position_resource.set_vec3(transform.translation());
        }
    }
}

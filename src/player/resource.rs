use amethyst::core::{math::Vector3, num::Zero, Float, Transform};

#[derive(new)]
pub struct PositionResource {
    position: Vector3<Float>,
}

impl From<&Vector3<Float>> for PositionResource {
    fn from(vector3: &Vector3<Float>) -> Self {
        PositionResource::new(vector3.clone())
    }
}

impl From<&Transform> for PositionResource {
    fn from(transform: &Transform) -> Self {
        PositionResource::new(transform.translation().clone())
    }
}

impl PositionResource {
    pub fn set<F: Into<Float>>(&mut self, x: F, y: F) {
        // TODO default z value
        self.position = Vector3::new(x.into(), y.into(), Float::zero());
    }

    pub fn set_vec3(&mut self, vector: &Vector3<Float>) {
        self.set(vector.x, vector.y);
    }

    pub fn position(&self) -> Vector3<Float> {
        self.position
    }

    pub fn vec3(&self) -> Vector3<Float> {
        self.position
    }
}

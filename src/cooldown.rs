use amethyst::ecs::{Component, DenseVecStorage};

use std::time::Duration;

// TODO unit tests.
// come on. it's easy.

pub struct Cooldown {
    status: Status,
    duration: Duration,
    time: Duration,
}

#[derive(Clone, Debug)]
pub enum Status {
    Waiting,
    Started,
}

impl From<Duration> for Cooldown {
    fn from(duration: Duration) -> Cooldown {
        return Cooldown {
            duration: duration,
            time: Duration::new(0, 0),
            status: Status::Waiting,
        };
    }
}

impl Cooldown {
    /// Move the timer forward by `delta` time.
    /// Returns the Status of the Cooldown.
    pub fn tick(&mut self, delta: Duration) -> Status {
        match self.status {
            Status::Started => match self.time + delta {
                x if x > self.duration => {
                    return Status::Waiting;
                }
                x => {
                    self.time = x;
                    return Status::Started;
                }
            },
            Status::Waiting => return Status::Waiting,
        }
    }

    pub fn reset(&mut self) {
        self.status = Status::Started;
        self.time *= 0;
    }
}

impl Component for Cooldown {
    type Storage = DenseVecStorage<Self>;
}

use amethyst::{
    core::Transform,
    ecs::{Entities, Join, LazyUpdate, Read, ReadExpect, ReadStorage, System, WriteStorage},
};

use super::{Background, BackgroundTile};

pub struct BackgroundSystem;
impl<'a> System<'a> for BackgroundSystem {
    type SystemData = (
        Entities<'a>,
        Read<'a, LazyUpdate>,
        ReadExpect<'a, Background>,
        ReadStorage<'a, BackgroundTile>,
        WriteStorage<'a, Transform>,
    );

    fn run(
        &mut self,
        (entities, lazy_update, background, background_tile, mut transform): Self::SystemData,
    ) {
        // TODO enable scrolling background
        // for (_tile, transform) in (&background_tile, &mut transform).join() {
        //     transform.move_left(background.scroll_speed);
        // }
    }
}

use rand;
use rand::Rng;

use amethyst::{
    core::math::Vector3, core::Transform, ecs::World, prelude::Builder, renderer::SpriteRender,
};

use super::BackgroundTile;
use super::BACKGROUND_FRAME_NUM;
use super::BACKGROUND_Z;

type Frames = Vec<SpriteRender>;

#[derive(new)]
pub struct Background {
    frames: Frames,
    pub scroll_speed: f32,
    pub tile_size: f32,
}

impl Background {
    pub fn next(&self) -> SpriteRender {
        let mut rng = rand::thread_rng();
        let i = rng.gen_range(0, BACKGROUND_FRAME_NUM);
        return self.frames[i].clone();
    }

    pub fn populate_background(&self, world: &mut World, width_tiles: usize, height_tiles: usize) {
        (0..width_tiles).for_each(|x| {
            (0..height_tiles).for_each(|y| {
                let x = (x as f32) * self.tile_size;
                let y = (y as f32) * self.tile_size;
                self.create_tile(world, x, y);
            })
        });
    }

    fn create_tile(&self, world: &mut World, x: f32, y: f32) {
        let mut transform = Transform::default();
        transform.set_translation_xyz(x, y, BACKGROUND_Z);

        let sprite = self.next();

        world
            .create_entity()
            .with(transform)
            .with(sprite)
            .with(BackgroundTile)
            .build();
    }
}

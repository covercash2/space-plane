use std::fs::File;
use std::io::BufReader;
use std::time::Duration;

use amethyst::{
    assets::{AssetStorage, Handle, Loader},
    core::transform::Transform,
    prelude::*,
    renderer::{
        Camera, Format, ImageFormat, SpriteRender, SpriteSheet, SubpassBuilder, Texture,
        Transparent,
    },
};

use crate::{
    background,
    background::Background,
    cooldown::Cooldown,
    enemy,
    enemy::Enemy,
    get_resource,
    named_sprite_sheet::{NamedSpriteSheetFormat, NamedSprites},
    physics::{init as init_physics, Collider, Object as PhysicalObject},
    player,
    player::Player,
    projectiles::{Projectile, ProjectileResource},
    weapon::Weapon,
};
use sheep::SerializedNamedSpriteSheet;

pub const DEFAULT_CAMERA_WIDTH: f32 = 960.0;
pub const DEFAULT_CAMERA_HEIGHT: f32 = 540.0;

pub struct GameState;
impl SimpleState for GameState {
    fn on_start(&mut self, data: StateData<'_, GameData<'_, '_>>) {
        let world = data.world;

        register_components(world);

        let (names, sprite_sheet_handle) =
            load_sprite_sheet(world, "resources/sprites.png", "resources/sprites.ron");

        let named_sprites = NamedSprites::new(&names, sprite_sheet_handle.clone());

        debug!("init");

        let mut collider = Collider::default();

        background::init(world, &named_sprites);
        player::init(world, &mut collider, &named_sprites);
        enemy::init(world, &named_sprites);

        initialize_camera(world);

        world.add_resource(collider);
    }
}

fn register_components(world: &mut World) {
    world.register::<Cooldown>();
    world.register::<Enemy>();
    world.register::<PhysicalObject>();
    world.register::<Player>();
    world.register::<Projectile>();
    world.register::<Transparent>();
    world.register::<Weapon>();
}

fn initialize_camera(world: &mut World) {
    let mut transform = Transform::default();
    transform.set_translation_x(DEFAULT_CAMERA_WIDTH / 2.0);
    transform.set_translation_y(DEFAULT_CAMERA_HEIGHT / 2.0);
    transform.set_translation_z(1.0);
    world
        .create_entity()
        .with(Camera::standard_2d(
            DEFAULT_CAMERA_WIDTH,
            DEFAULT_CAMERA_HEIGHT,
        ))
        .with(transform)
        .build();
}

pub fn load_sprite_sheet(
    world: &mut World,
    png_path: &str,
    ron_path: &str,
) -> (Vec<String>, Handle<SpriteSheet>) {
    let texture_handle = {
        let loader = world.read_resource::<Loader>();
        let texture_storage = world.read_resource::<AssetStorage<Texture>>();
        loader.load(png_path, ImageFormat::default(), (), &texture_storage)
    };

    let names = ::ron::de::from_reader::<_, SerializedNamedSpriteSheet>(BufReader::new(
        File::open("resources/sprites.ron").expect("could not open sprite sheet"),
    ))
    .expect("could not load sprite sheet")
    .sprites
    .into_iter()
    .map(|sprite| sprite.name)
    .collect();

    let loader = world.read_resource::<Loader>();
    let sprite_sheet_store = world.read_resource::<AssetStorage<SpriteSheet>>();
    return (
        names,
        loader.load(
            ron_path,
            NamedSpriteSheetFormat(texture_handle),
            (),
            &sprite_sheet_store,
        ),
    );
}

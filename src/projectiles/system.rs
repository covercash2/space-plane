use amethyst::{
    core::Transform,
    ecs::{Join, ReadStorage, System, WriteStorage},
};

use crate::projectiles::Projectile;

pub struct ProjectileSystem;

impl<'a> System<'a> for ProjectileSystem {
    type SystemData = (ReadStorage<'a, Projectile>, WriteStorage<'a, Transform>);

    fn run(&mut self, (projectile, mut transform): Self::SystemData) {
        for (projectile, transform) in (&projectile, &mut transform).join() {
            match projectile {
                Projectile::Shell(shell) => {
                    transform.append_translation_xyz(shell.speed.x, shell.speed.y, 0.0);
                }
            }
        }
    }
}

use amethyst::{
    assets::{Handle, Prefab},
    renderer::SpriteRender,
};

use super::Projectile;

type Component = Handle<Prefab<Projectile>>;

#[derive(Clone)]
pub struct ProjectileResource {
    pub component: Component,
    pub sprite: SpriteRender,
}

impl ProjectileResource {
    pub fn new(component: Component, sprite: SpriteRender) -> Self {
        return ProjectileResource {
            component: component,
            sprite: sprite,
        };
    }
}

#[derive(Clone)]
pub struct Shell {
    sprite: SpriteRender,
}

use amethyst::{
    assets::{Format, Handle},
    error::Error,
    renderer::{Sprite, SpriteRender, SpriteSheet, Texture},
};
use ron::de::from_bytes as from_ron_bytes;
use serde::{Deserialize, Serialize};
use sheep::SerializedNamedSpriteSheet;

#[derive(new, Clone)]
pub struct NamedSprites<'a> {
    names: &'a Vec<String>,
    sprite_sheet: Handle<SpriteSheet>,
}

impl<'a> NamedSprites<'a> {
    pub fn get(&self, name: &str) -> Option<SpriteRender> {
        return get_id(name, self.names).map(|id| SpriteRender {
            sprite_number: id,
            sprite_sheet: self.sprite_sheet.clone(),
        });
    }
}

fn get_id(name: &str, names: &Vec<String>) -> Option<usize> {
    return names
        .iter()
        .enumerate()
        .find(|(_, it)| it.as_str() == name)
        .map(|(i, _)| i);
}

#[derive(Clone, Deserialize, Serialize)]
pub struct SpriteNamesFormat;

#[derive(Clone, Debug)]
pub struct NamedSpriteSheetFormat(pub Handle<Texture>);

impl Format<SpriteSheet> for NamedSpriteSheetFormat {
    fn name(&self) -> &'static str {
        "named_sprite_sheet"
    }

    fn import_simple(&self, bytes: Vec<u8>) -> Result<SpriteSheet, Error> {
        let sheet: SerializedNamedSpriteSheet = from_ron_bytes(&bytes)
            .map_err(|_| Error::from_string("unable to deserialize ron file"))?;

        let (width, height) = (
            sheet.spritesheet_width as u32,
            sheet.spritesheet_height as u32,
        );

        let sprite_sheet = {
            let sprites: Vec<Sprite> = sheet
                .sprites
                .into_iter()
                .map(|named_sprite| {
                    Sprite::from_pixel_values(
                        width,
                        height,
                        named_sprite.width as u32,
                        named_sprite.height as u32,
                        named_sprite.x as u32,
                        named_sprite.y as u32,
                        named_sprite.offsets.unwrap_or([0.0; 2]),
                        false, // flip vertical
                        false, // flip horizontal
                    )
                })
                .collect();

            match self {
                NamedSpriteSheetFormat(texture) => SpriteSheet {
                    texture: texture.clone(),
                    sprites: sprites,
                },
            }
        };

        return Ok(sprite_sheet);
    }
}

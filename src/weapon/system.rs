use amethyst::{
    core::timing::Time,
    ecs::{
        Entities, Join, LazyUpdate, Read, ReadExpect, ReadStorage, System, WriteExpect,
        WriteStorage,
    },
    input::{InputHandler, StringBindings},
};

use crate::{
    cooldown::{Cooldown, Status},
    lazy_world::LazyWorld,
    physics::Collider,
    player::PositionResource as PlayerPosition,
    projectiles::ProjectileResource,
    weapon::Weapon,
};

pub struct WeaponSystem;

impl<'a> System<'a> for WeaponSystem {
    type SystemData = (
        Entities<'a>,
        Read<'a, LazyUpdate>,
        Read<'a, Time>,
        WriteStorage<'a, Cooldown>,
        WriteExpect<'a, Collider>,
        ReadExpect<'a, PlayerPosition>,
        ReadExpect<'a, ProjectileResource>,
        Read<'a, InputHandler<StringBindings>>,
        ReadStorage<'a, Weapon>,
    );

    fn run(
        &mut self,
        (
            entities,
            lazy_update,
            time,
            mut cooldown,
            mut collider,
            player_pos,
            projectile_resource,
            input,
            weapon,
        ): Self::SystemData,
    ) {
        for (weapon, cooldown) in (&weapon, &mut cooldown).join() {
            let delta = time.delta_time();

            let world = LazyWorld::new(&entities, &lazy_update);

            if let Status::Waiting = cooldown.tick(delta) {
                if let Some(true) = input.action_is_down("shoot") {
                    let muzzle_position = player_pos.vec3() + weapon.muzzle_pos();
                    weapon.fire(
                        projectile_resource.sprite.clone(),
                        projectile_resource.component.clone(),
                        muzzle_position.clone(),
                        world.create_entity(),
                        &mut collider,
                    );
                    cooldown.reset();
                }
            }
        }
    }
}

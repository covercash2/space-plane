use amethyst::ecs::{world::LazyBuilder, Entities, LazyUpdate};

pub struct LazyWorld<'a, 'e> {
    entities: &'a Entities<'e>,
    lazy_update: &'a LazyUpdate,
}

impl<'a, 'e> LazyWorld<'a, 'e> {
    pub fn new(entities: &'a Entities<'e>, lazy_update: &'a LazyUpdate) -> LazyWorld<'a, 'e> {
        return LazyWorld {
            entities: entities,
            lazy_update: lazy_update,
        };
    }

    pub fn create_entity(&self) -> LazyBuilder {
        return self.lazy_update.create_entity(&self.entities);
    }
}

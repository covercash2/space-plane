use std::collections::HashMap;
use std::str::FromStr;

use amethyst::{
    core::{math::Vector2, Float, Transform},
    ecs::Entity,
    prelude::Builder,
    renderer::SpriteRender,
};
use ncollide2d::shape::{Cuboid, ShapeHandle};
use rand;
use rand::{
    distributions::{Distribution, Standard},
    Rng,
};

use super::{Enemy, EnemyKind};
use crate::{
    game::{DEFAULT_CAMERA_HEIGHT, DEFAULT_CAMERA_WIDTH},
    physics::Collider,
};

const TYPE_NUM: usize = 2;

pub struct EnemyResource {
    sprites: HashMap<EnemyKind, SpriteRender>,
    active: u32,
}

impl EnemyResource {
    pub fn new(sprites: HashMap<EnemyKind, SpriteRender>) -> Self {
        return EnemyResource {
            sprites: sprites,
            active: 0,
        };
    }

    pub fn spawn_random<B: Builder>(&mut self, builder: B, collider: &mut Collider) -> Entity {
        let mut rng = rand::thread_rng();
        let kind: EnemyKind = rng.gen();
        let (sprite, transform) = self
            .sprites
            .get(&kind)
            .map(|sprite| {
                let mut transform = Transform::default();
                let y = rng.gen::<f32>() * DEFAULT_CAMERA_HEIGHT;
                transform.set_translation_xyz(DEFAULT_CAMERA_WIDTH, y, 0.0);
                (sprite.clone(), transform)
            })
            .expect("unable to load random enemy sprite");
        // increment number of active enemies
        self.active += 1;

        let shape = ShapeHandle::new(Cuboid::new(Vector2::new(
            Float::from(1.0),
            Float::from(1.0),
        )));

        return collider
            .create_body(builder, shape, transform)
            .with(Enemy::new(kind))
            .with(sprite)
            .build();
    }

    pub fn num_active(&self) -> u32 {
        return self.active;
    }
}

impl Distribution<EnemyKind> for Standard {
    fn sample<R: Rng + ?Sized>(&self, rng: &mut R) -> EnemyKind {
        let type_num = TYPE_NUM;
        match rng.gen_range(0, type_num) {
            0 => EnemyKind::Eye,
            1 => EnemyKind::Zombai,
            _ => panic!("generated number out of range"),
        }
    }
}

impl FromStr for EnemyKind {
    type Err = &'static str;

    fn from_str(s: &str) -> Result<Self, Self::Err> {
        return match s {
            "monster_1" => Ok(EnemyKind::Eye),
            "zombai" => Ok(EnemyKind::Zombai),
            _ => Err("unknown enemy type"),
        };
    }
}

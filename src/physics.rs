use amethyst::{
    core::{
        components::Transform,
        math::{zero, Isometry2, Vector2, Vector3, U2, U3},
        Float,
    },
    ecs::{
        Component, DenseVecStorage, EntityBuilder, Join, Read, ReadExpect, ReadStorage, System,
        World, Write, WriteExpect, WriteStorage,
    },
    prelude::*,
};
use ncollide2d::{
    narrow_phase::ContactAlgorithm,
    query::ContactManifold,
    shape::ShapeHandle,
    world::{CollisionGroups, CollisionObjectHandle, GeometricQueryType},
};

const DEFAULT_COLLISION_MARGIN: f32 = 0.02;

type CollisionWorld = ::ncollide2d::world::CollisionWorld<Float, ()>;

#[derive(new)]
pub struct Object {
    handle: CollisionObjectHandle,
    kind: ObjectKind,
}

/// a physical object
#[repr(u8)]
#[derive(Debug, Clone, PartialOrd, PartialEq)]
pub enum ObjectKind {
    Wall,
    Projectile,
    Body,
}

impl Component for Object {
    type Storage = DenseVecStorage<Self>;
}

impl Default for ObjectKind {
    fn default() -> Self {
        return ObjectKind::Body;
    }
}

impl Object {
    fn handle(&self) -> CollisionObjectHandle {
        return self.handle;
    }
}

pub struct Collider {
    groups: CollisionGroups,
    world: CollisionWorld,
}

impl Collider {
    fn update(&mut self) {
        self.world.update();
    }

    pub fn contacts(
        &self,
    ) -> impl Iterator<
        Item = (
            CollisionObjectHandle,
            CollisionObjectHandle,
            &ContactAlgorithm<Float>,
            &ContactManifold<Float>,
        ),
    > {
        self.world.contact_pairs(true)
    }

    pub fn create_body<'a, B: Builder>(
        &mut self,
        builder: B,
        shape: ShapeHandle<Float>,
        transform: Transform,
    ) -> B {
        self.create_object(builder, shape, ObjectKind::Body, transform)
    }

    pub fn create_object<'a, B: Builder>(
        &mut self,
        builder: B,
        shape: ShapeHandle<Float>,
        kind: ObjectKind,
        transform: Transform,
    ) -> B {
        create_collidable(
            builder,
            &mut self.world,
            &self.groups,
            shape,
            kind,
            transform,
        )
    }

    fn set_position(&mut self, handle: CollisionObjectHandle, transf: &Transform) {
        self.world.set_position(handle, isometry2d(transf));
    }
}

impl Default for Collider {
    fn default() -> Collider {
        Collider {
            groups: CollisionGroups::default(),
            world: CollisionWorld::new(Float::from(DEFAULT_COLLISION_MARGIN)),
        }
    }
}

pub struct CollisionSystem;
impl<'a> System<'a> for CollisionSystem {
    type SystemData = (
        WriteExpect<'a, Collider>,
        ReadStorage<'a, Object>,
        ReadStorage<'a, Transform>,
    );

    fn run(&mut self, (mut collider, object, transform): Self::SystemData) {
        for (object, transform) in (&object, &transform).join() {
            collider.set_position(object.handle(), transform);
        }
        collider.update();

        for data in collider.contacts() {
            debug!("contact");
        }
    }
}

fn isometry2d(transf: &Transform) -> Isometry2<Float> {
    Isometry2::new(transf.translation().xy(), transf.rotation().angle())
}

fn create_collidable<'a, B: Builder>(
    builder: B,
    collider: &mut CollisionWorld,
    groups: &CollisionGroups,
    shape: ShapeHandle<Float>,
    object_kind: ObjectKind,
    transform: Transform,
) -> B {
    let handle = collider
        .add(
            isometry2d(&transform),
            shape,
            *groups,
            GeometricQueryType::Contacts(zero(), zero()),
            (),
        )
        .handle();

    let object = Object::new(handle, object_kind);

    builder.with(transform).with(object)
}

// NOTE: spawn system instead of init fns?
// bundles?
//
pub fn init(world: &mut World) {
    world.add_resource(Collider::default());
}

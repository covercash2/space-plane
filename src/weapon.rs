use amethyst::{
    assets::{Handle, Prefab, PrefabData, PrefabLoader, RonFormat},
    core::{
        math::{Vector2, Vector3},
        num::Zero,
        Float, Transform,
    },
    ecs::{Component, DenseVecStorage, Entity, World, WriteStorage},
    error::Error,
    prelude::Builder,
    renderer::SpriteRender,
};
use ncollide2d::shape::{Cuboid, ShapeHandle};
use serde::Deserialize;

use crate::{lazy_world::LazyWorld, physics::Collider};

mod system;

pub use system::WeaponSystem;

#[derive(Clone, Deserialize, PrefabData)]
#[prefab(Component)]
pub struct Weapon {
    kind: WeaponKind,
    muzzle_pos: Vector3<Float>,
}

#[derive(Clone, Deserialize)]
pub enum WeaponKind {
    Repeater,
}

impl Component for Weapon {
    type Storage = DenseVecStorage<Self>;
}

impl Weapon {
    pub fn load(world: &mut World, prefab_path: &str) -> Handle<Prefab<Weapon>> {
        return world
            .exec(|loader: PrefabLoader<'_, Weapon>| loader.load(prefab_path, RonFormat, ()));
    }

    pub fn muzzle_pos(&self) -> Vector3<Float> {
        self.muzzle_pos
    }

    pub fn fire<P>(
        &self,
        sprite: SpriteRender,
        projectile: P,
        plane_position: Vector3<Float>,
        builder: impl Builder,
        collider: &mut Collider,
    ) where
        P: Component + Sync + Send,
    {
        let transform: Transform = plane_position.into();

        let shape = ShapeHandle::new(Cuboid::new(Vector2::new(
            Float::from(1.0),
            Float::from(1.0),
        )));

        collider
            .create_body(builder, shape, transform)
            .with(projectile)
            .with(sprite)
            .build();
    }
}

use amethyst::{
    core::Time,
    ecs::{Join, Read, System, WriteStorage},
};

use super::Cooldown;

pub struct CooldownSystem;
impl<'a> System<'a> for CooldownSystem {
    type SystemData = (Read<'a, Time>, WriteStorage<'a, Cooldown>);
    fn run(&mut self, (time, mut cooldown): Self::SystemData) {
        for cooldown in (&mut cooldown).join() {
            ::log::debug!("tick");
            cooldown.tick(time.delta_time());
        }
    }
}

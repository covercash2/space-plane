use std::time::Duration;

use amethyst::{
    core::{math::Vector2, Float, Transform},
    ecs::{Component, DenseVecStorage, World},
    prelude::Builder,
};

use ncollide2d::shape::{Cuboid, ShapeHandle};

use crate::{
    cooldown::Cooldown,
    get_resource,
    named_sprite_sheet::NamedSprites,
    physics::Collider,
    projectiles::{Projectile, ProjectileResource},
    weapon::Weapon,
};

mod resource;
mod system;

pub use resource::PositionResource;
pub use system::PlayerSystem;

const PLAYER_SPRITE_NAME: &str = "player_base";

pub struct Player;
impl Component for Player {
    type Storage = DenseVecStorage<Self>;
}

pub fn init(world: &mut World, collider: &mut Collider, sprites: &NamedSprites) {
    let mut transform = Transform::default();
    transform.set_translation_xyz(50.0, 50.0, -1.0);

    let player = Player;
    let player_sprite = sprites
        .get(PLAYER_SPRITE_NAME)
        .expect("could not find player sprite");
    let weapon = Weapon::load(world, &get_resource("prefab/weapons.ron"));
    let weapon_cooldown: Cooldown = Duration::from_millis(100).into();

    let projectile = Projectile::load(world, &get_resource("prefab/pea_shell.ron"));

    let projectile_sprite = sprites.get("bullet").expect("could not find bullet sprite");
    world.add_resource(ProjectileResource {
        component: projectile,
        sprite: projectile_sprite,
    });

    let position_resource = PositionResource::from(&transform);
    world.add_resource(position_resource);

    let shape = ShapeHandle::new(Cuboid::new(Vector2::new(
        Float::from(1.0),
        Float::from(1.0),
    )));

    collider
        .create_body(world.create_entity(), shape, transform)
        .with(player)
        .with(player_sprite)
        .with(weapon)
        .with(weapon_cooldown)
        .build();
}

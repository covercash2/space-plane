use amethyst::ecs::{Component, NullStorage, World};

use crate::named_sprite_sheet::NamedSprites;

mod resource;
mod system;

pub use resource::Background;
pub use system::BackgroundSystem;

const MAX_WIDTH_TILES: usize = 30;
const MAX_HEIGHT_TILES: usize = 17;
const TILE_SIZE: f32 = 32.0;
const BACKGROUND_Z: f32 = -10.0;

const BACKGROUND_FRAME_NUM: usize = 16;
pub const BACKGROUND_STAR_NAMES: [&str; BACKGROUND_FRAME_NUM] = [
    "bg_stars_32x32_0",
    "bg_stars_32x32_1",
    "bg_stars_32x32_2",
    "bg_stars_32x32_3",
    "bg_stars_32x32_4",
    "bg_stars_32x32_5",
    "bg_stars_32x32_6",
    "bg_stars_32x32_7",
    "bg_stars_32x32_8",
    "bg_stars_32x32_9",
    "bg_stars_32x32_10",
    "bg_stars_32x32_11",
    "bg_stars_32x32_12",
    "bg_stars_32x32_13",
    "bg_stars_32x32_14",
    "bg_stars_32x32_15",
];

pub struct BackgroundTile;
impl Component for BackgroundTile {
    type Storage = NullStorage<BackgroundTile>;
}
impl Default for BackgroundTile {
    fn default() -> Self {
        return BackgroundTile;
    }
}

pub fn init(world: &mut World, sprites: &NamedSprites) {
    let sprites = BACKGROUND_STAR_NAMES
        .iter()
        .map(|star_name| sprites.get(star_name).expect("sprite does not exist"))
        .collect();

    const DEFAULT_SCROLL_SPEED: f32 = 1.0;
    let scroll_speed = DEFAULT_SCROLL_SPEED;

    let background = Background::new(sprites, scroll_speed, TILE_SIZE);

    background.populate_background(world, MAX_WIDTH_TILES, MAX_HEIGHT_TILES);

    world.add_resource(background);
}

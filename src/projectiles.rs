use amethyst::{
    assets::{Handle, Prefab, PrefabData, PrefabLoader, RonFormat},
    core::math::Vector2,
    ecs::{Component, DenseVecStorage, Entity, World, WriteStorage},
    error::Error,
};
use serde::Deserialize;

mod resource;
mod system;

pub use resource::ProjectileResource;
pub use system::ProjectileSystem;

#[derive(Clone, Deserialize, PrefabData)]
#[prefab(Component)]
pub enum Projectile {
    Shell(Shell),
}

impl Projectile {
    pub fn load(world: &mut World, prefab_path: &str) -> Handle<Prefab<Projectile>> {
        return world
            .exec(|loader: PrefabLoader<'_, Projectile>| loader.load(prefab_path, RonFormat, ()));
    }
}

#[derive(Clone, Deserialize)]
pub struct Shell {
    pub name: String,
    pub speed: Vector2<f32>,
}

impl Component for Projectile {
    type Storage = DenseVecStorage<Self>;
}

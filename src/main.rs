#![feature(custom_attribute)]
#[macro_use]
extern crate amethyst;
#[macro_use]
extern crate log;
#[macro_use]
extern crate derive_new;

mod background;
mod cooldown;
mod enemy;
mod game;
mod lazy_world;
mod named_sprite_sheet;
mod physics;
mod player;
mod projectiles;
mod renderer;
mod weapon;

use std::path::PathBuf;

use amethyst::{
    assets::{PrefabLoaderSystem, Processor},
    core::transform::TransformBundle,
    input::{InputBundle, StringBindings},
    prelude::*,
    renderer::{types::DefaultBackend, RenderingSystem, SpriteSheet},
    utils::application_root_dir,
    window::WindowBundle,
    LogLevelFilter, Logger,
};

use game::GameState;

use background::BackgroundSystem;
use enemy::{MoveSystem as EnemyMoveSystem, SpawnSystem};
use physics::CollisionSystem;
use player::PlayerSystem;
use projectiles::{Projectile, ProjectileSystem};
use renderer::Graph as RenderGraph;
use weapon::{Weapon, WeaponSystem};

const DEFAULT_RESOURCE_DIR: &str = "./resources";

pub fn get_resource(path: &str) -> String {
    application_root_dir()
        .map(|root: PathBuf| {
            root.join(DEFAULT_RESOURCE_DIR)
                .join(path)
                .canonicalize()
                .expect("unable to resolve resource directory")
        })
        .expect("unable to get application root directory")
        .to_string_lossy()
        .into_owned()
}

fn main() -> amethyst::Result<()> {
    Logger::from_config(Default::default())
        .level_for("gfx_backend_vulkan", LogLevelFilter::Warn)
        .level_for("spaceplane", LogLevelFilter::Debug)
        .start();

    let window_config_path = get_resource("display_config.ron");

    let bindings_path = "./resources/bindings_config.ron";

    let input_bundle =
        InputBundle::<StringBindings>::new().with_bindings_from_file(bindings_path)?;

    let game_data = GameDataBuilder::default()
        .with_bundle(WindowBundle::from_config_path(&window_config_path))?
        .with_bundle(input_bundle)?
        .with_bundle(TransformBundle::new())?
        .with(
            PrefabLoaderSystem::<Projectile>::default(),
            "projectile_prefab_loader",
            &[],
        )
        .with(
            PrefabLoaderSystem::<Weapon>::default(),
            "weapon_prefab_loader",
            &[],
        )
        .with(
            Processor::<SpriteSheet>::new(),
            "sprite_sheet_processor",
            &[],
        )
        .with_thread_local(RenderingSystem::<DefaultBackend, _>::new(
            RenderGraph::default(),
        ))
        .with(CollisionSystem, "collision_system", &["transform_system"])
        .with(WeaponSystem, "weapon_system", &["input_system"])
        .with(BackgroundSystem, "background_system", &[])
        .with(ProjectileSystem, "projectile_system", &[])
        .with(SpawnSystem, "enemy_spawn_system", &[])
        .with(EnemyMoveSystem, "enemy_move_system", &[])
        .with(PlayerSystem, "player_system", &["input_system"]);
    let mut game = Application::new("./", GameState, game_data)?;

    game.run();

    Ok(())
}

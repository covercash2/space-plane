use std::collections::HashMap;
use std::str::FromStr;

use amethyst::{
    core::{
        math::{Vector2, Vector3},
        transform::components::Transform,
        Float,
    },
    ecs::{
        Component, DenseVecStorage, Entities, Entity, Join, LazyUpdate, Read, ReadExpect,
        ReadStorage, System, World, Write, WriteExpect, WriteStorage,
    },
    prelude::Builder,
    renderer::SpriteRender,
};

use crate::{
    lazy_world::LazyWorld, named_sprite_sheet::NamedSprites, physics::Collider,
    player::PositionResource as PlayerPosition,
};

mod resource;

pub use resource::EnemyResource;

const ENEMY_SPRITE_NUM: usize = 2;
const ENEMY_SPRITE_NAMES: [&str; ENEMY_SPRITE_NUM] = ["monster_1", "zombai"];

#[derive(Debug, PartialEq, Eq, Hash)]
pub enum EnemyKind {
    Eye,
    Zombai,
}

#[derive(new)]
pub struct Enemy {
    kind: EnemyKind,
}
pub struct MoveSystem;
pub struct SpawnSystem;

impl Component for Enemy {
    type Storage = DenseVecStorage<Self>;
}

impl<'a> System<'a> for MoveSystem {
    type SystemData = (
        ReadStorage<'a, Enemy>,
        WriteStorage<'a, Transform>,
        ReadExpect<'a, PlayerPosition>,
    );

    fn run(&mut self, (enemy, mut transform, player_pos): Self::SystemData) {
        for (_enemy, transform) in (&enemy, &mut transform).join() {
            let enemy_pos: Vector3<Float> = *transform.translation();
            let direction: Vector3<Float> = (player_pos.position() - enemy_pos)
                // TODO magic number
                .normalize()
                * Float::from(0.1);

            transform.append_translation(direction);
        }
    }
}

impl<'a> System<'a> for SpawnSystem {
    type SystemData = (
        Entities<'a>,
        Read<'a, LazyUpdate>,
        WriteExpect<'a, EnemyResource>,
        WriteExpect<'a, Collider>,
    );

    fn run(&mut self, (entities, lazy_update, mut enemy_resource, mut collider): Self::SystemData) {
        let world = LazyWorld::new(&entities, &lazy_update);
        // TODO make smarter enemy spawner
        if enemy_resource.num_active() == 0 {
            enemy_resource.spawn_random(world.create_entity(), &mut collider);
        }
    }
}

pub fn init(world: &mut World, named_sprites: &NamedSprites) {
    let mut map = HashMap::new();
    ENEMY_SPRITE_NAMES.iter().for_each(|name| {
        let sprite = named_sprites
            .get(name)
            .expect("could not find sprite for enemy name");

        map.insert(
            EnemyKind::from_str(name).expect("could not find enemy type for name"),
            sprite,
        );
    });

    let resource = EnemyResource::new(map);
    world.add_resource(resource);
}
